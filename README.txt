
INTRODUCTION:
------------
This module provides feature for importing fields, 
from a csv file into a content type.

For a full description of the module, visit the project page:

https://www.drupal.org/sandbox/jack_ry/2788849

REQUIREMENTS:
-------------
This module requires the following modules:

field
field_ui

INSTALLATION:
-------------
1. Place the entire field import folder into your Drupal sites/all/modules/
   directory.

2. Enable the field import module by navigating to:

     administer > modules

3. Once the module is installed
   you will find a Field Import tab in the admin menubar.

IMPORTANT NOTE:
---------------
1. Use the template xlsx file for adding field names.

2. After adding field names for import
   make sure you save the template file into csv format and then upload it.

Author:
-------
Jack Ry
